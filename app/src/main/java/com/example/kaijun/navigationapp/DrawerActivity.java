package com.example.kaijun.navigationapp;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class DrawerActivity extends AppCompatActivity {

    //Defining Variables
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private final static String TAG = "DrawerActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        // Initializing Toolbar and setting it as the actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Set up Firebase
        Firebase.setAndroidContext(this);

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.navigation_view);


        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {


                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.home:
                        //Toast.makeText(getApplicationContext(), "Home Selected", Toast.LENGTH_SHORT).show();
                        HomeFragment fragment = new HomeFragment();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.frame, fragment);
                        fragmentTransaction.commit();
                        return true;

                    case R.id.drive:
                        //Toast.makeText(getApplicationContext(), "Drive Selected", Toast.LENGTH_SHORT).show();
                        DriveFragment driveFragment = new DriveFragment();
                        FragmentTransaction driveFragmentTransaction = getFragmentManager().beginTransaction();
                        driveFragmentTransaction.replace(R.id.frame,driveFragment);
                        driveFragmentTransaction.commit();
                        return true;

                    case R.id.booklist:
                        BooklistFragment booklistFragment = new BooklistFragment();
                        FragmentTransaction booklistFragmentTransaction = getFragmentManager().beginTransaction();
                        booklistFragmentTransaction.replace(R.id.frame,booklistFragment);
                        booklistFragmentTransaction.commit();
                        return true;

                    case R.id.vote:
                        String link = "https://docs.google.com/forms/d/1lUjBkrKcXdkK11SbFMn9I-9kww0RAxsCv3tvEy8kkyw/viewform";
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                        startActivity(browserIntent);
                        return true;

                    case R.id.nomination:
                        NominationFragment nominationFragment = new NominationFragment();
                        FragmentTransaction nominationFragmentTransaction = getFragmentManager().beginTransaction();
                        nominationFragmentTransaction.replace(R.id.frame,nominationFragment);
                        nominationFragmentTransaction.commit();
                        return true;

                    case R.id.logout:
                        //Toast.makeText(getApplicationContext(), "Logout Selected", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent (DrawerActivity.this, MainActivity.class);
                        intent.putExtra("sign_out",true);
                        startActivity(intent);
                        return true;

                    default:
                        Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        //set default fragment
        FragmentTransaction tx = getFragmentManager().beginTransaction();
        tx.replace(R.id.frame,new HomeFragment());
        tx.commit();

        //set up profile
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String email = intent.getStringExtra("email");
        Uri photo = Uri.parse(intent.getStringExtra("photo"));

        View headerLayout = navigationView.getHeaderView(0);
        TextView name_view = (TextView) headerLayout.findViewById(R.id.username);
        TextView email_view = (TextView) headerLayout.findViewById(R.id.email);
        CircleImageView photo_view = (CircleImageView) headerLayout.findViewById(R.id.profile_image);

        name_view.setText(name);
        email_view.setText(email);
        Picasso.with(this).load(photo).into(photo_view);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
