package com.example.kaijun.navigationapp;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kaijun on 12/7/2015.
 */
public class NominationFragment extends Fragment implements View.OnClickListener{
    //private static final String TAG = "NominationFragment";
    //private View fragmentView;
    String currentNomination;
    EditText txtTitle;
    EditText txtAuthor;
    Button btnNominate;
    TextView txtThanks;
    Button btnBack;
    Firebase ref = new Firebase("https://glowing-heat-4909.firebaseio.com/");
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.nomination_fragment,container,false);
        txtAuthor = (EditText) v.findViewById(R.id.editAuthor);
        txtThanks = (TextView) v.findViewById(R.id.textMerci);
        txtTitle = (EditText) v.findViewById(R.id.editTitle);
        btnNominate = (Button) v.findViewById(R.id.nominationButton);
        btnBack = (Button) v.findViewById(R.id.buttonBack);
        btnNominate.setOnClickListener(this);
        //v.findViewById(R.id.nominationButton).setOnClickListener(this);
        //v.findViewById(R.id.backButton).setOnClickListener(this);
        //fragmentView = v;
        ref.child("current-keys/current-nomination").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                currentNomination = dataSnapshot.getValue().toString();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Toast.makeText(getActivity(),firebaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nominationButton :{
                nominateBook();
                break;
            }
            case R.id.buttonBack :{
                updateUI(false);
                break;
            }
            /*
            case R.id.sign_out_button: {
                signOut();
                break;
            }
            */
        }
    }

    private void nominateBook(){
        //EditText txtTitle = (EditText) fragmentView.findViewById(R.id.editTitle);
        //EditText txtAuthor = (EditText) fragmentView.findViewById(R.id.editAuthor);
        String strTitle = txtTitle.getText().toString();
        String strAuthor = txtAuthor.getText().toString();
        if (strTitle.isEmpty()) {
            txtTitle.setError("Empty");
            return;
        }
        if (strAuthor.isEmpty()) {
            txtAuthor.setError("Empty");
            return;
        }
        Firebase postRef = ref.child("nominations/"+currentNomination);
        Map<String,String> post = new HashMap<>();
        post.put("author",strAuthor);
        post.put("title",strTitle);
        postRef.push().setValue(post);
        Toast.makeText(getActivity(),"Book nominated",Toast.LENGTH_SHORT).show();
        updateUI(true);
    }

    private void updateUI(boolean nominating) {
        //EditText txtTitle = (EditText) fragmentView.findViewById(R.id.editTitle);
        //EditText txtAuthor = (EditText) fragmentView.findViewById(R.id.editAuthor);
        //Button btnNominate = (Button) fragmentView.findViewById(R.id.nominationButton);
        if (nominating) {
            txtAuthor.setVisibility(View.GONE);
            txtAuthor.getText().clear();
            txtTitle.setVisibility(View.GONE);
            txtTitle.getText().clear();
            btnNominate.setVisibility(View.GONE);
            btnBack.setVisibility(View.VISIBLE);
            txtThanks.setVisibility(View.VISIBLE);
            btnBack.setOnClickListener(this);
        }
        else {
            txtAuthor.setVisibility(View.VISIBLE);
            txtTitle.setVisibility(View.VISIBLE);
            btnNominate.setVisibility(View.VISIBLE);
            btnBack.setVisibility(View.GONE);
            txtThanks.setVisibility(View.GONE);
        }
    }
}
