package com.example.kaijun.navigationapp;

/**
 * Created by Kaijun on 11/28/2015.
 */
public class BooklistItem {
    private String title;
    private String author;

    public BooklistItem(String title, String author) {
        this.author = author;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
