package com.example.kaijun.navigationapp;

import android.app.ListFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

/**
 * Created by Kaijun on 11/28/2015.
 */
public class BooklistFragment extends ListFragment implements AdapterView.OnItemClickListener{

    List<String> bookTitles;
    List<String> bookAuthors;
    Firebase firebaseRef = new Firebase("https://glowing-heat-4909.firebaseio.com/");
    TextView booklistTitle;
    List<String> links;

    BooklistAdapter adapter;
    private List<BooklistItem> rowItems;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.booklist_fragment, container, false);
        booklistTitle = (TextView) view.findViewById(R.id.booklist_title);
        firebaseRef.child("current-keys/current-month").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String temp = dataSnapshot.getValue().toString();
                booklistTitle.setText("Book list of " + temp);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Toast.makeText(getActivity(),firebaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }
    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bookTitles = Arrays.asList("Thomas Jefferson and the Tripoli Pirates",
                "War Letters: Extraordinary Correspondence from American Wars",
                "Washington: A Life",
                "A Stillness at Appomattox");
        bookAuthors = Arrays.asList("Brian Kilmeade and Don Yaeger",
                "Andrew Carroll",
                "Ron Chernow",
                "Bruce Catton");
        links = Arrays.asList("http://www.amazon.com/Thomas-Jefferson-Tripoli-Pirates-Forgotten-ebook/dp/B00SI0B5GW",
                "http://www.amazon.com/War-Letters-Extraordinary-Correspondence-American-ebook/dp/B00304XCNU",
                "http://www.amazon.com/Washington-Life-Ron-Chernow-ebook/dp/B003ZK58SQ",
                "http://www.amazon.com/Stillness-Appomattox-Army-Potomac-Trilogy-ebook/dp/B004AM5R16");
        rowItems = new ArrayList<>();
        for (int i = 0; i<bookAuthors.size(); i++) {
            BooklistItem item = new BooklistItem(bookTitles.get(i),"by "+bookAuthors.get(i));
            rowItems.add(item);
        }

        adapter = new BooklistAdapter(getActivity(),rowItems);
        setListAdapter(adapter);
        //List<String> planets = Arrays.asList("Mercury", "Venus", "Earth");
        //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, planets);
        //setListAdapter(arrayAdapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String link = links.get(position);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(browserIntent);
    }
}
