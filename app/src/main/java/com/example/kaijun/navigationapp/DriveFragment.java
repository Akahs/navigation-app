package com.example.kaijun.navigationapp;

import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.ChildList;
import com.google.api.services.drive.model.ChildReference;
import com.google.api.services.drive.model.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Created by Kaijun on 11/27/2015.
 */
public class DriveFragment extends ListFragment
        implements AdapterView.OnItemClickListener,
        View.OnClickListener{

    GoogleAccountCredential mCredential;
    ProgressDialog mProgress;
    static final String BOOKCLUB_FOLDER_ID = "0BwN9rC35YBjHaWxjUmlHMVhobGc";
    String folderId = BOOKCLUB_FOLDER_ID;
    Stack<String> viewedFolderId = new Stack<>();
    List<File> files = new ArrayList<>();
    static final String MIMETYPE_FOLDER = "application/vnd.google-apps.folder";
    static final int RESULT_OK = -1;
    static final int RESULT_CANCELED = 0;
    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {DriveScopes.DRIVE_READONLY};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Calling Drive API...");

        // Initialize stack
        //viewedFolderId.push(BOOKCLUB_FOLDER_ID);

        // Initialize credentials and service object.
        SharedPreferences settings = getActivity().getPreferences(Context.MODE_PRIVATE);
        mCredential = GoogleAccountCredential.usingOAuth2(
                getActivity().getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(settings.getString(PREF_ACCOUNT_NAME, null));

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isGooglePlayServicesAvailable()) {
            refreshResults();
        } else {
            Toast.makeText(getActivity(), "Google Play Services required: " +
                    "after installing, close and relaunch this app.", Toast.LENGTH_SHORT).show();
        }
    }

    private void refreshResults() {
        if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else {
            if (isDeviceOnline()) {
                new MakeRequestTask(mCredential).execute();
            } else {
                Toast.makeText(getActivity(), "No network connection available.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void chooseAccount() {
        startActivityForResult(
                mCredential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
    }

    @Override
    public void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    isGooglePlayServicesAvailable();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        mCredential.setSelectedAccountName(accountName);
                        SharedPreferences settings =
                                getActivity().getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(getActivity(), "Account unspecified.", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode != RESULT_OK) {
                    chooseAccount();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drive_fragment, container, false);
        view.findViewById(R.id.backButton).setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*
        List<String> planets = Arrays.asList("Mercury", "Venus", "Earth");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, planets);
        setListAdapter(arrayAdapter);
        */
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Toast.makeText(getActivity(), "Position " + position, Toast.LENGTH_SHORT).show();
        File selectedFile = files.get(position);
        //Toast.makeText(DriveActivity.this,"Mime type: "+selectedFile,Toast.LENGTH_SHORT).show();
        String resourceId = selectedFile.getId();
        String mimeType = selectedFile.getMimeType();
        if (mimeType.equals(MIMETYPE_FOLDER))
        {
            //Intent intent = new Intent(DriveActivity.this, DriveActivity.class);
            //intent.putExtra("folderId",resourceId);
            //startActivity(intent);
            viewedFolderId.push(folderId);
            folderId = resourceId;
            refreshResults();
        }
        else
        {
            String link = selectedFile.getAlternateLink();
            //Toast.makeText(DriveActivity.this,link,Toast.LENGTH_SHORT).show();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            startActivity(browserIntent);
        }
    }

    private boolean isGooglePlayServicesAvailable() {
        final int connectionStatusCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        } else if (connectionStatusCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                connectionStatusCode,
                getActivity(),
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.backButton) {
            //Toast.makeText(getActivity(), "back...",Toast.LENGTH_SHORT).show();
            //String topId = viewedFolderId.peek();
            if (!viewedFolderId.empty()) {
                folderId = viewedFolderId.pop();
                refreshResults();
            }
        }
    }

    private class MakeRequestTask extends AsyncTask<Void, Void, Void> {
        private com.google.api.services.drive.Drive mService = null;
        private Exception mLastError = null;

        public MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.drive.Drive.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Drive API Android Quickstart")
                    .build();
        }

        /**
         * Background task to call Drive API.
         * @param params no parameters needed for this task.
         */
        @Override
        protected Void doInBackground(Void... params) {
            //Log.i(TAG,"DoInBackGround...");
            try {
                getDataFromApi(folderId);
                return null;
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch a list of up to 10 file names and IDs.
         * @return List of Strings describing files, or an empty list if no files
         *         found.
         * @throws IOException
         */
        private void getDataFromApi(String folderId) throws IOException {
            // Get a list of up to 10 files.
            //List<File> files = new ArrayList<>();
            File file;
            ChildList result = mService.children().list(folderId)
                    .setMaxResults(10)
                    .execute();
            List<ChildReference> children = result.getItems();
            //if (!files.isEmpty()) {
                files.clear();
            //}
            if (children != null) {
                for (ChildReference child : children) {
                    String fileId = child.getId();
                    file = mService.files().get(fileId).execute();
                    //fileInfo.add(String.format("%s (%s)\n",
                    //        file.getTitle(), child.getId()));
                    files.add(file);
                }
            }
            //return files;
        }


        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        @Override
        protected void onPostExecute(Void params) {
            //Log.i(TAG, "log info: Post execute...");
            mProgress.hide();
            //ListView folderListView = (ListView) findViewById(R.id.listView);
            if (files == null || files.size() == 0) {
                Toast.makeText(getActivity(), "No results returned.", Toast.LENGTH_SHORT).show();
            }
            else {
                List<String> fileInfo = getFileNames(files);
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, fileInfo);
                setListAdapter(arrayAdapter);
                //final List<String> fileNames = fileInfo;
                /*
                final List<File> final_files = files;
                folderListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        File selectedFile = final_files.get(position);
                        //Toast.makeText(DriveActivity.this,"Mime type: "+selectedFile,Toast.LENGTH_SHORT).show();
                        String resourceId = selectedFile.getId();
                        String mimeType = selectedFile.getMimeType();
                        if (mimeType.equals(MIMETYPE_FOLDER))
                        {
                            Intent intent = new Intent(DriveActivity.this, DriveActivity.class);
                            intent.putExtra("folderId",resourceId);
                            startActivity(intent);
                        }
                        else
                        {
                            String link = selectedFile.getAlternateLink();
                            //Toast.makeText(DriveActivity.this,link,Toast.LENGTH_SHORT).show();
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                            startActivity(browserIntent);
                        }
                    }
                });
                */
            }

        }

        private List<String> getFileNames(List<File> files) {
            List<String> fileNames = new ArrayList<>();
            for (File file : files) {
                fileNames.add(file.getTitle());
            }
            return fileNames;
        }

        @Override
        protected void onCancelled() {
            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            DriveFragment.REQUEST_AUTHORIZATION);
                } else {
                    Toast.makeText(getActivity(),"The following error occurred:\n"
                            + mLastError.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getActivity(), "Request cancelled.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
